window.onscroll = function(event) {
    let navbar = document.querySelector(".presto-navbar")
    //console.log(document.documentElement.scrollTop)
    if (document.documentElement.scrollTop > 100) {
        navbar.classList.remove("padding-extension")
    } else {
        navbar.classList.add("padding-extension")
    }
}

let categories = [
    {
        titolo: "abbigliamento",
        icona: "tshirt",
    },
    {
        titolo: "Case",
        icona: "home",
    },
    {
        titolo: "motociclette",
        icona: "motorcycle",
    },
    {
        titolo: "automobili",
        icona: "car",
    },
    {
        titolo: "razzi",
        icona: "rocket",
    },
    {
        titolo: "giochi",
        icona: "gamepad",
    },
    {
        titolo: "portatili",
        icona: "laptop",
    },
    {
        titolo: "portatili",
        icona: "laptop",
    },
    
] 

categories.forEach(function(category) {
    let categoryElement = document.createElement("div")
    categoryElement.className = "presto-card presto-text text-center d-flex flex-column shadow m-1"

    categoryElement.innerHTML =
    `
        <i class="fas fa-${category.icona} fa-3x"></i>
        <p class="h4 mt-3">
         ${category.titolo}
         </p>
    `
    categoryElement.addEventListener("mouseenter", function() {
        categoryElement.classList.remove("shadow")
        categoryElement.classList.add("shadow-lg")
    })
    categoryElement.addEventListener("mouseleave", function() {
        categoryElement.classList.remove("shadow-lg")
        categoryElement.classList.add("shadow")
    })

    let categoriesWrapper = document.querySelector("#categories-wrapper")
    categoriesWrapper.appendChild(categoryElement)
})


/*
let categoriesCards = document.querySelectorAll("#categories-wrapper .presto-card")

categoriesCards.forEach(function(categoryCards) {

})
*/

let announcements = [
    {
        utente: "user12",
        fotoUrl: "https://picsum.photos/200",
        annuncio: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam officia voluptate exercitationem sequi libero eum dolorem qui incidunt quia aspernatur aut, quae in nulla eligendi. Illum sint ullam quaerat ut"
    },
    {
        utente: "user12",
        fotoUrl: "https://picsum.photos/200",
        annuncio: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam officia voluptate exercitationem sequi libero eum dolorem qui incidunt quia aspernatur aut, quae in nulla eligendi. Illum sint ullam quaerat ut"
    },
    {
        utente: "user24",
        fotoUrl: "https://picsum.photos/200",
        annuncio: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam officia voluptate exercitationem sequi libero eum dolorem qui incidunt quia aspernatur aut, quae in nulla eligendi. Illum sint ullam quaerat ut"
    },
    {
        utente: "userX",
        fotoUrl: "https://picsum.photos/200",
        annuncio: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam officia voluptate exercitationem sequi libero eum dolorem qui incidunt quia aspernatur aut, quae in nulla eligendi. Illum sint ullam quaerat ut"
    },
    
    
] 

announcements.forEach(function(announcement) {
    let announcementElement = document.createElement("div")
    announcementElement.classList.add ("glide__slide")
    
    announcementElement.innerHTML =
    `
        <h4 class="fw-bolder">${announcement.utente}</h4>
        <img class="rounded rounded-3" src= "${announcement.fotoUrl}">
        <p class="mt-3">
         ${announcement.annuncio}
         </p>
    `
    
    let glideSlidesWrapper = document.querySelector(".glide .glide__track .glide__slides")
    glideSlidesWrapper.appendChild(announcementElement)
})








